# Nyttig

## What is Nyttig?

Nyttig is a personal micro server with a handful of simple PHP-based applications:

- Radio (stream radio stations)
- RSS (RSS aggregator)
- Editor (scratchpad with rudimentary versioning)
- Files (upload and share files)
- Dashboard (basic system info)

## Why precisely these applications?

Nyttig is a [WFH](https://www.dictionary.com/browse/wfh) companion. When I work, I like to listen to the radio. I check my favorite RSS feeds during my lunch break, and I occasionally jot down notes and share a file or a photo.

## Can't you just use a smartphone for that?

Sure I can.

## Why did you build Nyttig, then?

Rummaging though boxes of hardware, I stumbled upon an original version Raspberry Pi Model B. I decided to use it as a dedicated radio streaming machine. To control it, I cobbled together a simple PHP-based web UI. Then I decided to add a simple editor for scribbling down notes. One thing led to another, and here we are.

## Aren't you reinventing the wheel?

Most definitely! But it's an excellent learning experience and a lot of fun.

## What makes Nyttig special?

- Nyttig requires zero installation
- It can run on practically any Linux machine with PHP
- It's simple and easy to tweak, improve, and extend
- Nyttig is resource-efficient, and runs perfectly well on modest hardware like Raspberry Pi 1 Model B.

## How do I deploy Nyttig?

First of all, make sure that no other services are running on port 80. To install Nyttig on a Raspberry Pi OS, use the following command:

    curl -sSL https://gitlab.com/dmpop/nyttig/-/raw/master/install-nyttig.sh | bash

If you prefer to install Nyttig manually, make sure that Git and PHP is installed on the Linux machine that is going to serve Nyttig. Assuming your machine runs a Debian-based Linux distribution, run the following commands:

```
sudo apt update
sudo apt install git php-cli php-xml php-json jq mplayer sox
git clone https://gitlab.com/dmpop/nyttig.git
cd nyttig
nano config.php # Change the default values
sudo php -S 0.0.0.0:80
```

To start Nyttig automatically on boot, add the following cronjob (replace _/path/to/_ with the actual path to Nyttig):

    @reboot cd /path/to/nyttig && sudo php -S 0.0.0.0:80

## How about running Nyttig in a container?

Install Podman, then run the following commands to build an image and run a container:

```
cd nyttig
podman build -t nyttig .
podman run --device /dev/snd -it -p8000:8000 --rm nyttig
```

To add persistent storage, specify the desired volumes:

    podman run --rm --device /dev/snd -d -p8000:8000 -v /path/to/local/config.php:/usr/src/nyttig/config.php:ro --rm nyttig

Point the browser to http://127.0.0.1:8000 (replace _127.0.0.1_ with the actual IP address of the machine running the container).

## By the way, what does Nyttig mean?

Nyttig means _helpful or serving a purpose well_ in Danish.

## How can I contribute?

If you've found a bug or have a suggestion for improvements, open an issue in the [Issues](https://gitlab.com/dmpop/nyttig/issues) section.

To add a new feature or fix issues yourself, follow the following steps.

1. Fork the project's repository
2. Create a feature branch using the `git checkout -b new-feature` command
3. Add your new feature or fix bugs and run the `git commit -am 'Add a new feature'` command to commit changes
4. Push changes using the `git push origin new-feature` command
5. Submit a pull request

## Author

Dmitri Popov [dmpop@linux.com](mailto:dmpop@linux.com)

## License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)