<?php
include('parsedown.php');
include('../config.php');
if ($PROTECT) {
	require_once('../protect.php');
}
?>
<html lang="en">
<!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>Nyttig DID</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/uikit.min.css" />
	<script src="../js/uikit.min.js"></script>
	<script src="../js/uikit-icons.min.js"></script>
</head>

<body>
	<div class="uk-container uk-margin-small-top">
		<div class="uk-card uk-card-primary uk-card-body">
			<h1 class="uk-heading-line uk-text-center"><span>Nyttig DID</span></h1>
			<form method='post' action=''>
				<label for='note'>Today I did:</label>
				<textarea class="uk-textarea" name="did"></textarea>
				<button class="uk-button uk-button-primary uk-margin-top" type='submit' role='button' name='add'>Add</button>
			</form>
		</div>
		<?php
		if (!file_exists("data")) {
			mkdir("data", 0777, true);
		}
		$date = date('Y-m-d');
		if (!empty($_POST["did"])) {
			$did = $_POST["did"];
		} else {
			$did = "";
		}
		if (isset($_POST['add'])) {
			$f = fopen("data/" . $date . ".md", "a");
			fwrite($f, $did . "\n");
			fclose($f);
		}
		//$files = scandir("data");
		//Get a list of file paths using the glob function.
		$flist = array_reverse(glob('data/*.md'));
		//Loop through the array that glob returned.
		foreach ($flist as $f) {
			$fname = basename($f, ".md");
			echo "<div class='uk-container uk-margin-small-top'>";
			echo "<div class='uk-card uk-card-default uk-card-body'>";
			echo "<h3 class='uk-heading-line uk-text-center'><span>" . $fname . "</span></h3>";
			$file_contents = file_get_contents($f, true);
			$Parsedown = new Parsedown();
			echo $Parsedown->text($file_contents);
			echo "</div>";
		}
		?>
	</div>
</body>

</html>