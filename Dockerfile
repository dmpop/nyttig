FROM alpine:latest
LABEL maintainer="dmpop@linux.com"
LABEL version="0.1"
LABEL description="Nyttig container image"
RUN apk update
RUN apk add procps git mplayer sox jq php-cli php-xml php-json alsa-utils
COPY . /usr/src/nyttig
WORKDIR /usr/src/nyttig
EXPOSE 8000
CMD [ "php", "-S", "0.0.0.0:8000" ]