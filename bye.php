<html lang="en">

<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
    <title>Nyttig Dashboard</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/uikit.min.css" />
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>
</head>

<body>
    <div class="uk-container uk-container-xsmall uk-margin-top uk-text-center">
        <h1 class="uk-heading-line uk-text-center"><span>Bye!</span></h1>
        <img class="uk-text-center" data-src="wtf-cow.jpg" alt="" uk-img>
        <p>Press <em>Shut down</em> to power off the server.</p>
        <form class="uk-margin-top uk-margin-bottom uk-text-center" action=" " method="POST">
            <button class="uk-button uk-button-danger" name="poweroff">Shut down</button>
        </form>
        <?php
        if (isset($_POST['poweroff'])) {
            shell_exec('sudo poweroff > /dev/null 2>&1 & echo $!');
        }
        ?>
        <hr>
    </div>
    <p class="uk-text-center uk-margin-bottom">This is <a href="https://gitlab.com/dmpop/nyttig">Nyttig</a></p>
</body>

</html>